import store from '../store'
import 'firebase/compat/storage';

const uploadFile = (file, id, path) => {  
  const firbaseObj = store.getters.firebase
  var extension = file.name.split('.').pop();
  var storage = firbaseObj.storage();
  var storageRef = storage.ref();
  var destRef = storageRef.child( path + "/" + id + '.' + extension);
  return destRef.put(file);
};

const deleteFile = (name, path) => {
  const firbaseObj = store.getters.firebase
  var storage = firbaseObj.storage();
  var storageRef = storage.ref();
  var destRef = storageRef.child( path + "/" + name);
  return destRef.delete()
}

export { uploadFile, deleteFile };
