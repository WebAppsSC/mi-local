import { getDatabase, ref, get, child, set, push } from "firebase/database";

const getAllCategories = () => {
  return new Promise(function(resolve, reject) {
    const dbRef = ref(getDatabase());
    get(child(dbRef, "/admin/categories/"))
      .then((snapshot) => {
        if (snapshot.exists()) {
          if (snapshot.val()) {
            resolve(snapshot.val());
          } else {
            reject([]);
          }
        } else {
          reject([]);
        }
      })
      .catch((error) => {
        console.error(error);
        reject([]);
      });
  });
};

const addCategory = (name, description, file, color) => {
  return new Promise(function(resolve, reject) {
    const db = getDatabase();
    const postListRef = ref(db, "/admin/categories/");
    const newPostRef = push(postListRef);

    var extension = file.name.split('.').pop();

    set(newPostRef, {
      name: name,
      description: description,
      thumbnail: newPostRef.key + '.' + extension,
      color: color
    })
      .then(() => {
        resolve(newPostRef.key);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

const getCategoryInfo = (uid) => {
  const db = getDatabase();
  const dbref = ref(db, "/admin/categories/" + uid)
  return get(dbref);  
};



export { getAllCategories, addCategory, getCategoryInfo };
