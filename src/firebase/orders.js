import {
  getDatabase,
  ref,
  set,
  push,
  get,
  query,
  orderByChild,
  equalTo,
  update
} from "firebase/database";

const newOrder = (data) => {
  return new Promise(function(resolve, reject) {
    const db = getDatabase();
    const postListRef = ref(db, "/admin/orders/");
    const newPostRef = push(postListRef);
    set(newPostRef, data)
      .then(() => {
        resolve(newPostRef.key);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

const getOrder = (order_id) => {
  const db = getDatabase();
  const dbref = ref(db, "/admin/orders/" + order_id)
  return get(dbref);  
};

const getClientOrders = (client_id) => {
  const db = getDatabase();
  const dbref = ref(db, "/admin/orders/")    
  return get(query(dbref, ...[orderByChild("uid"), equalTo(client_id)]));    
};

const getStoreOrders = (store_id) => {
  const db = getDatabase();
  const dbref = ref(db, "/admin/orders/")    
  return get(query(dbref, ...[orderByChild("storeUid"), equalTo(store_id)]));    
};

const updateOrderEntry = (oid, key, value) => {
  const db = getDatabase();
  const updates = {};
  updates["/admin/orders/" + oid + "/" + key] = value;
  return update(ref(db), updates);
}

export { newOrder, getOrder,getClientOrders, getStoreOrders, updateOrderEntry };
