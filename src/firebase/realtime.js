import { getDatabase, ref, onValue, query, orderByChild, equalTo } from "firebase/database";

const getOrdersRT = (store_id, callback) => {
  const db = getDatabase();
  const dbref = ref(db, "/admin/orders/")
  const queryOrders = query(dbref, ...[orderByChild("storeUid"), equalTo(store_id)])
  onValue(queryOrders, callback);
};

const getUserOrdersRT = (store_id, callback) => {
  const db = getDatabase();
  const dbref = ref(db, "/admin/orders/")
  const queryOrders = query(dbref, ...[orderByChild("uid"), equalTo(store_id)])
  onValue(queryOrders, callback);
};

export { getOrdersRT, getUserOrdersRT };
