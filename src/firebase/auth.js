import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  updateProfile,
} from "firebase/auth";
import { getDatabase, ref, update, get, child } from "firebase/database";

const registerWithEmail = (email, password) => {
  const auth = getAuth();
  return createUserWithEmailAndPassword(auth, email, password);
};

const loginWithEmail = (email, password) => {
  const auth = getAuth();
  return signInWithEmailAndPassword(auth, email, password);
};


const updateUserToDBEmail = (user, role, name = '') => {
  return new Promise(function(resolve, reject) {
    const db = getDatabase();

    var postData = {      
      email: user.email,
      photo: '',
      phone: '',
      starCount: 5,
      role: role,
      uid: user.uid,
    };

    if(name) {
      postData["name"] = name;
    }

    const dbRef = ref(getDatabase());
    get(child(dbRef, "/admin/users/" + user.uid))
      .then((snapshot) => {
        if (snapshot.exists()) {
          if (snapshot.val()) {
            try {
              postData.role = snapshot.val().role;
              if (snapshot.val().role == "business") {
                if(snapshot.val().business) {
                  postData.business = snapshot.val().business;
                }
                if(snapshot.val().categories) {
                  postData.categories = snapshot.val().categories;
                }                
                if(snapshot.val().bcategory) {
                  postData.bcategory = snapshot.val().bcategory; 
                }                
                if(snapshot.val().creation_date) {
                  postData.creation_date = snapshot.val().creation_date;
                }                
              }
              if (snapshot.val().role == "client") {
                  postData.addresses = [];
                if(snapshot.val().addresses) {
                  postData.addresses = snapshot.val().addresses;
                }                
              }
              if (snapshot.val().phone) {
                postData.phone = '';
                if(snapshot.val().phone) {
                  postData.phone = snapshot.val().phone;
                }                
              }
            } catch (err) {
              console.warn("Welcome User", err)
            }
          } else {
            postData["creation_date"] = new Date().getTime();
          }
        }        
        const updates = {};
        updates["/admin/users/" + user.uid] = postData;
        update(ref(db), updates);
        resolve(true);
      })
      .catch((error) => {
        console.warn(error);
        reject(error);
      });
  });
};

const updateUserToDB = (user, role) => {
  return new Promise(function(resolve, reject) {
    const db = getDatabase();

    var postData = {
      name: user.displayName,
      email: user.email,
      photo: user.photoURL,
      starCount: 5,
      role: role,
      uid: user.uid,
    };

    const dbRef = ref(getDatabase());
    get(child(dbRef, "/admin/users/" + user.uid))
      .then((snapshot) => {
        if (snapshot.exists()) {
          if (snapshot.val()) {
            try {
              postData.role = snapshot.val().role;
              if (snapshot.val().role == "business") {
                postData.business = snapshot.val().business;
                postData.categories = snapshot.val().categories;
                postData.bcategory = snapshot.val().bcategory;
                postData.creation_date = snapshot.val().creation_date;
              }
              if (snapshot.val().role == "client") {
                  postData.addresses = [];
                if(snapshot.val().addresses) {
                  postData.addresses = snapshot.val().addresses;
                }                
              }
              if (snapshot.val().phone) {
                postData.phone = '';
                if(snapshot.val().phone) {
                  postData.phone = snapshot.val().phone;
                }                
              }
            } catch (err) {
              console.warn("Welcome User", err)
            }
          } else {
            postData["creation_date"] = new Date().getTime();
          }
        }        
        const updates = {};
        updates["/admin/users/" + user.uid] = postData;
        update(ref(db), updates);
        resolve(true);
      })
      .catch((error) => {
        console.warn(error);
        reject(error);
      });
  });
};

const getUserDB = (uid) => {
  return new Promise(function(resolve, reject) {
    const dbRef = ref(getDatabase());
    get(child(dbRef, "/admin/users/" + uid))
      .then((snapshot) => {
        if (snapshot.exists()) {
          if (snapshot.val()) {
            resolve(snapshot.val());
          } else {
            reject(false);
          }
        } else {
          reject(false);
        }
      })
      .catch((error) => {
        console.warn(error);
        reject(false);
      });
  });
};

const updataInfo = (name) => {
  const auth = getAuth();
  updateProfile(auth.currentUser, {
    displayName: name,
  });
};

/**
 * Administrators
 */
/*const loginAdmins = (email, password) => {
  const db = getDatabase();
  const starCountRef = ref(db, "posts/" + postId + "/starCount");
  onValue(starCountRef, (snapshot) => {
    const data = snapshot.val();
    updateStarCount(postElement, data);
  });
};*/

export {
  registerWithEmail,
  loginWithEmail,
  updataInfo,
  updateUserToDB,
  updateUserToDBEmail,
  getUserDB,
};
