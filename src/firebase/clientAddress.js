import { getDatabase, ref, get, update } from "firebase/database";

const getClientAddresses = (uid) => {
  const db = getDatabase();
  const dbref = ref(db, "/admin/users/" + uid + "/addresses");
  return get(dbref);
};

const updateAdresses = (uid, list) => {
  console.log("Updating", list);
  const db = getDatabase();
  const updates = {};
  updates["/admin/users/" + uid + "/addresses"] = list;
  return update(ref(db), updates);
};

export { getClientAddresses, updateAdresses };
