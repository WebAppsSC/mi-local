import { getFirestore, getDoc, doc  } from "firebase/firestore";
import { getFunctions, httpsCallable } from "firebase/functions";

const createRatingEntry = (rating, client) => {
  const functions = getFunctions();
  const addRating = httpsCallable(functions, "addRating");
  return addRating({
    rating: rating.value,
    comments: rating.comments,
    client: {
      name: client.name,
      uid: client.uid,
    },
    order: rating.order,
    creation_date: new Date().getTime(),
    store_id: rating.storeUid,
  });
};

const getStoreRating = (sid) => {
  const db = getFirestore();  
  const docRef = doc(db, "users", sid)
  return getDoc(docRef)
};

export { createRatingEntry, getStoreRating };
