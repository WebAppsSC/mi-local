import {
  getDatabase,
  ref,
  set,
  push,
  query,
  orderByChild,
  equalTo,
  get,
  remove,
} from "firebase/database";

const addProduct = (uid, name, price, description, image, category) => {
  return new Promise(function(resolve, reject) {
    const db = getDatabase();
    const postListRef = ref(db, "/admin/products/");
    const newPostRef = push(postListRef);

    var extension = image.name.split(".").pop();

    set(newPostRef, {
      name: name,
      description: description,
      thumbnail: newPostRef.key + "." + extension,
      price: price,
      category: category,
      uid: uid,
      created_date: new Date().getTime(),
    })
      .then(() => {
        resolve(newPostRef.key);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

const updateProduct = (pid, name, price, description, image, category, product) => {
  return new Promise(function(resolve, reject) {
    const db = getDatabase();
    const postListRef = ref(db, "/admin/products/" + pid);
    let saveImage = product.thumbnail
    console.log("IMAGE RESULT UPDATE", image)
    if(image != false) {
      var extension = image.name.split(".").pop();
      saveImage = pid + "." + extension
    }
    

    set(postListRef, {
      name: name,
      description: description,
      thumbnail: saveImage,
      price: price,
      category: category,
      uid: product.uid,
      created_date: product.created_date,
    })
      .then(() => {
        resolve(pid);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

const getUserProducts = (uid) => {
  const db = getDatabase();
  const dbref = ref(db, "/admin/products/")
  return get(query(dbref, ...[orderByChild("uid"), equalTo(uid)]));  
};

const deleteProduct = (pid) => {
  const db = getDatabase()
  const dbRef = ref(db, "/admin/products/" + pid)
  return remove(dbRef)
}



export { addProduct, getUserProducts,deleteProduct,updateProduct };
