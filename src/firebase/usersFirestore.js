import {
  getFirestore,
  doc,
  setDoc,  
  orderBy,
  query,
  collection,
  getDocs
} from "firebase/firestore";

const createOrUpdateUser = (uid, name, keywords, creation_date) => {
  const db = getFirestore();
  console.log("Update firestore")
  return setDoc(
    doc(db, "users", uid),
    {
      name: name,
      uid: uid,
      keywords: keywords,
      creation_date: creation_date,
    },
    { merge: true }
  );
};

const updateSettingsFireStore = (
  uid,
  name,
  categories,
  keywords,
  thumbnail,
  time,
  starCount
) => {
  const db = getFirestore();
  return setDoc(
    doc(db, "users", uid),
    {
      name: name,
      keywords: keywords,
      category: categories,
      banner: thumbnail,
      deliveryTime: time,
      starCount: starCount,
    },
    { merge: true }
  );
};

const getLastBusiness = () => {
  const db = getFirestore();
  const docRef = collection(db, "users");
  const q = query(docRef, orderBy("creation_date", "desc"));
  return getDocs(q);
};

export { createOrUpdateUser, updateSettingsFireStore, getLastBusiness };
