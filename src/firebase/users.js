import { getDatabase, ref, get, child, update, orderByChild, equalTo, query } from "firebase/database";

const getAll = () => {
  return new Promise(function(resolve, reject) {
    const dbRef = ref(getDatabase());
    get(child(dbRef, "/admin/users/"))
      .then((snapshot) => {
        if (snapshot.exists()) {
          if (snapshot.val()) {
            resolve(snapshot.val());
          } else {
            reject([]);
          }
        } else {
          reject([]);
        }
      })
      .catch((error) => {
        console.error(error);
        reject([]);
      });
  });
};

const convertTo = (user, role) => {
  const db = getDatabase();

  var postData = role;

  const updates = {};
  updates["/admin/users/" + user.uid + "/role"] = postData;
  return update(ref(db), updates);
};

const getBusinessInfo = (uid) => {
  return new Promise(function(resolve, reject) {
    const dbRef = ref(getDatabase());
    get(child(dbRef, "/admin/users/" + uid + "/business"))
      .then((snapshot) => {
        if (snapshot.exists()) {
          if (snapshot.val()) {
            resolve(snapshot.val());
          } else {
            reject(false);
          }
        } else {
          reject(false);
        }
      })
      .catch((error) => {
        console.error(error);
        reject(false);
      });
  });
};

const getBusinessCategories = (uid) => {
  return new Promise(function(resolve, reject) {
    const dbRef = ref(getDatabase());
    get(child(dbRef, "/admin/users/" + uid + "/categories"))
      .then((snapshot) => {
        if (snapshot.exists()) {
          if (snapshot.val()) {
            resolve(snapshot.val());
          } else {
            reject(false);
          }
        } else {
          reject(false);
        }
      })
      .catch((error) => {
        console.error(error);
        reject(false);
      });
  });
};

const updateBusinessInfo = (uid, info) => {
  console.log("Updating", info)
  const db = getDatabase();
  const updates = {};
  updates["/admin/users/" + uid + "/business"] = info;
  updates["/admin/users/" + uid + "/bcategory"] = info.category;
  return update(ref(db), updates);
};

const updateBusinessCategories = (uid, list) => {
  const db = getDatabase();
  const updates = {};
  updates["/admin/users/" + uid + "/categories"] = list;
  return update(ref(db), updates);
};


const getBusinessByCategory = (cid) => {
  const db = getDatabase();
  const dbref = ref(db, "/admin/users/")
  return get(query(dbref, ...[orderByChild("bcategory"), equalTo(cid)]));  
};


// Clients

const getClientInfo = (uid) => {
  const db = getDatabase();
  const dbref = ref(db, "/admin/users/" + uid)
  return get(dbref);  
};

const updateUserEntry = (uid, key, value) => {
  const db = getDatabase();
  const updates = {};  
  updates["/admin/users/" + uid + "/" + key] = value;
  return update(ref(db), updates);
}





export { getAll, convertTo, updateBusinessInfo, getBusinessInfo, getBusinessCategories, updateBusinessCategories, getClientInfo, updateUserEntry, getBusinessByCategory };
