import Vue from "vue";
import VueRouter from "vue-router";
import VueCookies from 'vue-cookies'

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Login",
    component: () => import("../views/Login.vue"),
  },
  {
    path: "/home",
    name: "Home",
    component: () => import("../views/Home.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/orders",
    name: "Orders",
    component: () => import("../views/Orders.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/account",
    name: "My Account",
    component: () => import("../views/MyAccount.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/store/:id",
    name: "Business",
    component: () => import("../views/BusinessProducts.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/checkout",
    name: "Checkout",
    component: () => import("../views/PreCheckout.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/order/:id",
    name: "Order",
    component: () => import("../views/Order.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/category/:id",
    name: "Category",
    component: () => import("../views/Category.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/search",
    name: "Search",
    component: () => import("../views/Search.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  // Admin
  {
    path: "/admin",
    name: "Admin Login",
    component: () => import("../views/Admin/Home.vue"),
    meta: {
      requiresAuth: true,
      onlyAdmin: true,
    },
    redirect: "/admin/general",
  },
  {
    path: "/admin/general",
    name: "General",
    component: () => import("../views/Admin/Home.vue"),
    meta: {
      requiresAuth: true,
      onlyAdmin: true,
    },
  },
  {
    path: "/admin/users",
    name: "Users",
    component: () => import("../views/Admin/Users.vue"),
    meta: {
      requiresAuth: true,
      onlyAdmin: true,
    },
  },
  {
    path: "/admin/statistics",
    name: "Statistics",
    component: () => import("../views/Admin/Statistics.vue"),
    meta: {
      requiresAuth: true,
      onlyAdmin: true,
    },
  },
  {
    path: "/admin/account",
    name: "Account",
    component: () => import("../views/Admin/Account.vue"),
    meta: {
      requiresAuth: true,
      onlyAdmin: true,
    },
  },
  {
    path: "/admin/membership/:id",
    name: "UserMembership",
    component: () => import("../views/Admin/Membership.vue"),
    meta: {
      requiresAuth: true,
      onlyAdmin: true,
    },
  },
  // Business
  {
    path: "/business",
    name: "Business",
    component: () => import("../views/Business/Dashboard.vue"),
    meta: {
      requiresAuth: true,
      onlyBusiness: true,
    },
    redirect: "/business/dashboard",
  },
  {
    path: "/business/dashboard",
    name: "Dashboard",
    component: () => import("../views/Business/Dashboard.vue"),
    meta: {
      requiresAuth: true,
      onlyBusiness: true,
    },
  },
  {
    path: "/business/settings",
    name: "BusinessSettings",
    component: () => import("../views/Business/Settings.vue"),
    meta: {
      requiresAuth: true,
      onlyBusiness: true,
    },
  },
  {
    path: "/business/orders",
    name: "BusinessOrders",
    component: () => import("../views/Business/Orders.vue"),
    meta: {
      requiresAuth: true,
      onlyBusiness: true,
    },
  },
  {
    path: "/business/products",
    name: "BusinessProducts",
    component: () => import("../views/Business/Products.vue"),
    meta: {
      requiresAuth: true,
      onlyBusiness: true,
    },
  },
  {
    path: "/business/categories",
    name: "BusinessCategories",
    component: () => import("../views/Business/Categories.vue"),
    meta: {
      requiresAuth: true,
      onlyBusiness: true,
    },
  },
  {
    path: "/business/order/:id",
    name: "Order",
    component: () => import("../views/Business/Order.vue"),
    meta: {
      requiresAuth: true,
      onlyBusiness: true,
    },
  },
  {
    path: "/business/statistics",
    name: "BusinessStatistics",
    component: () => import("../views/Business/Statistics.vue"),
    meta: {
      requiresAuth: true,
      onlyBusiness: true,
    },
  },
  {
    path: "/business/services",
    name: "BusinessServices",
    component: () => import("../views/Business/Services.vue"),
    meta: {
      requiresAuth: true,
      onlyBusiness: true,
    },
  },
];


const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  setTimeout(() => {
    window.scrollTo(0, 0)
  },500)
  const role = VueCookies.get("role");
  
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    
    if(to.meta.onlyAdmin) {
      if(role != "admin") {
        next("/home")
      }
    }

    if(to.meta.onlyBusiness) {
      if(role != "business") {
        next("/home")
      }
    }

    if (to.name == "Login") {
      if (role == "business") {
        if (VueCookies.get("business_init") == "false") {
          next("/business/orders");
        } else {
          next("/business/settings");
        }
      } else {
        next("/home");
      }
    } else {
      if (role == "business") {
        if (VueCookies.get("business_init") == "false") {
          next();
        } else {
          if (to.path != "/business/settings") {
            next("/business/settings");
          } else {
            next();
          }
        }
      } else {
        next();
      }
    }
  } else {
    if (role != null) {
      if (to.path == "/") {
        if (role == "business") {
          next("/business/orders");
        } else if(role == "admin") {
          next("/admin");
        } else if(role == "client") {
          next("/home");
        } else {
          next();  
        }
      } else {
        next();
      }
    } else {
      next();
    }
  }
});

export default router;
