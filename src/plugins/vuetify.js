import Vue from "vue";
import Vuetify from "vuetify/lib/framework";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#F45B55",
        secondary: "#FFD863",
        accent: '#5446D9'
      },
    },
  },
});
