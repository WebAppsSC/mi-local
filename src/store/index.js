import Vue from "vue";
import Vuex from "vuex";
import { getAuth, signOut } from "firebase/auth";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    logged: false,    
    photo: null,
    role: 'client',
    forceViewAdmin: false,
    firebase: null,
    userInfo: false,
    enableToolbar: true,
    // Cart
    cartOpen: false,
    cart: [],
    cartTotals: 0.00,
    cartDetails: false,
    // Shipping
    shipAddress: false,
    // Orders
    activeOrder: false,
    // RealTime
    ordersList: [] 
  },
  mutations: {
    setLogged(state, value) {
      state.logged = value;
    },
    setPhoto(state, value) {
      state.photo = value;
    },
    setRole(state, value) {
      state.role = value;
    },
    forceViewAdmin(state, value) {
      state.forceViewAdmin = value;
    },
    setFirebase(state, value) {
      state.firebase = value;
    },
    setUserInfo(state, value) {
      state.userInfo = value;
    },
    enableToolbar(state, value) {
      state.enableToolbar = value;
    },
    cartStatus(state, value) {
      state.cartOpen = value
    },
    setCartDetails(state, value) {
      state.cartDetails = value
    },
    emptyCart(state) {
      state.cart = []
    },
    setCart(state, value) {
      state.cart = value
      let totals = 0
      for(const p of state.cart) {
        totals+=parseFloat(parseFloat(p.price) * parseInt(p.quantity))
      }
      state.cartTotals = totals.toFixed(2)
    },
    ADD_Item(state, item) {
      state.cart.push(item)
      let totals = 0
      for(const p of state.cart) {
        totals+=parseFloat(parseFloat(p.price) * parseInt(p.quantity))
      }
      state.cartTotals = totals.toFixed(2)
    },
    REMOVE_Item(state, index) {
      state.cart.splice(index,1)
      let totals = 0
      for(const p of state.cart) {
        totals+=parseFloat(parseFloat(p.price) * parseInt(p.quantity))
      }
      state.cartTotals = totals.toFixed(2)
    },
    // Shipping
    setShipAddress(state, value) {
      state.shipAddress = value
    },
    setActiveOrder(state, value) {
      state.activeOrder = value
    },
    // RealTime
    setOrdersList(state, value) {
      state.ordersList = value
    },
  },
  actions: {
    setLogged({ commit }, value) {
      commit("setLogged", value);
    },
    setPhoto({ commit }, value) {
      commit("setPhoto", value);
    },
    setRole({ commit }, value) {
      commit("setRole", value);
    },
    forceViewAdmin({ commit }, value) {
      commit("forceViewAdmin", value);
    },
    setFirebase({ commit }, value) {
      commit("setFirebase", value);
    },
    setUserInfo({ commit }, value) {
      commit("setUserInfo", value);
    },
    enableToolbar({ commit }, value) {
      commit("enableToolbar", value);
    },
    // Cart
    cartStatus({ commit }, value) {
      commit("cartStatus", value);
    },
    setCartDetails({ commit }, value) {
      commit("setCartDetails", value);
    },
    emptyCart({ commit }) {
      commit("emptyCart");
    },
    setCart({ commit }, value) {
      commit("setCart", value);
    },
    addItem(context, item) {
      context.commit("ADD_Item", item);      
    },
    removeItem(context, index) {
      context.commit("REMOVE_Item", index);
    },
    // Shipping
    setShipAddress({ commit}, value) {
      commit("setShipAddress", value);
    },
    // Orders
    setActiveOrder({ commit}, value) {
      commit("setActiveOrder", value);
    },
    // RealTime
    setOrdersList({ commit}, value) {
      commit("setOrdersList", value);
    },
    // Logout
    logout({ commit }) {
      const auth = getAuth();
      signOut(auth)
        .then(() => {
          commit("setLogged", false);
        })
        .catch((error) => {
          console.log("Error in logout", error);
          commit("setLogged", false);
        });
    },
  },
  modules: {},
  getters: {
    isLogged: (state) => state.logged,
    userPhoto: (state) => state.photo,
    getRole: (state) => state.role,
    getForceStatus: (state) => state.forceViewAdmin,
    firebase: (state) => state.firebase,
    user: (state) => state.userInfo,    
    enableToolbar: (state) => state.enableToolbar,    
    cart: (state) => state.cart,
    cartTotals: (state) => state.cartTotals,
    isCartOpen: (state) => state.cartOpen,
    getCartDetails: (state) => state.cartDetails,
    getShipAdresss: (state) => state.shipAddress,
    getActiveOrder: (state) => state.activeOrder,
    // Realtime Gettres
    getOrdersList: (state) => state.ordersList,
  },
});
