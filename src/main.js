import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import { initializeApp } from 'firebase/app';
import { getAnalytics } from "firebase/analytics";
import { getFirestore } from "firebase/firestore"
import VuePageTransition from 'vue-page-transition'
import firebase from "firebase/compat/app";
import VueCookies from 'vue-cookies'
import VueSmoothScroll from 'vue2-smooth-scroll'
import * as VueGoogleMaps  from 'vue2-google-maps'
import * as dayjs from 'dayjs'
import VueNativeNotification from 'vue-native-notification'
import { getFunctions } from 'firebase/functions';
import Ads from 'vue-google-adsense'
import '@mdi/font/css/materialdesignicons.min.css'
import './assets/scss/global.scss'



Vue.use(require('vue-script2'))

Vue.use(Ads.AutoAdsense, { adClient: 'ca-pub-4386412746374485' })
 
var relativeTime = require('dayjs/plugin/relativeTime')
dayjs.extend(relativeTime)
require('dayjs/locale/es')
dayjs.locale('es')

Vue.use(VueNativeNotification, {
  // Automatic permission request before
  // showing notification (default: true)
  requestOnNotify: true
})
 
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAb0j7hT7u6CXXkMinxFNCwXlP0oIiQwtg'    
  },
  language: 'es',
  installComponents: true
})


Vue.use(VueSmoothScroll, {
  duration: 400,
  updateHistory: false,
})
Vue.use(VueCookies)
Vue.use(VuePageTransition)


const firebaseConfig = {
  apiKey: "AIzaSyBoFNtUK97aNw8lA0Ph3Ctx1GnslT2Y3nM",
  authDomain: "milocal-5c651.firebaseapp.com",
  projectId: "milocal-5c651",
  storageBucket: "milocal-5c651.appspot.com",
  messagingSenderId: "1025924317829",
  appId: "1:1025924317829:web:b2ebafe907b7c170e69fec",
  measurementId: "G-79X6F9VNX6",
  databaseURL: "https://milocal-5c651-default-rtdb.firebaseio.com",
};

// Initialize Firebase
initializeApp(firebaseConfig);
const firbaseObj = firebase.initializeApp(firebaseConfig, "StorageCompact");
store.dispatch("setFirebase", firbaseObj)
getAnalytics()
getFirestore();
getFunctions();

Vue.config.productionTip = false;

// Filtros
Vue.filter('formatDate', function (value) {
  const from = dayjs(value).from(dayjs())
  return from + ', ' + dayjs(value).format('hh:mm a')
})

Vue.filter('timeFromDate', function (value) {  
  const d1 = dayjs(value)
  const num = dayjs().diff(d1, 'minute')  

  return 'hace ' +num + ' ' + ((num < 2) ? 'minuto  ' : 'minutos');           
})

Vue.filter('formatDateSmall', function (value) {  
  return dayjs(value).format('DD MMM YY')
})

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
