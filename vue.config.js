module.exports = {  
  transpileDependencies: [
    'vuetify'
  ],
  pwa: {
    name: 'Mi Local',
    themeColor: '#F45B55',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
    manifestOptions: {
      background_color: "#F45B55"
    }
  }
}
